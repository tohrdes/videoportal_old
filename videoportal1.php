<?php
/**
 * @package videoportal
 * @version 1
 */
/*
Plugin Name: Videoportal
Plugin URI: 
Description: Folgende Top-Level Seiten müssen erzeugt werden: event, video
Author: Tobias Ohrdes
Version: 1.0
Author URI: 
*/

//[vp_event]
function vp_event_shortcode( $atts ){
	$code = get_query_var( 'event_id' );
	global $wpdb;
	$html =		'<h3>Event Code eingeben:</h3>
				<form action="" method="GET" style="display:inline">
					<input type="text" name="event_id" maxlength="20" />
					<input type="submit" value="Los"/>
				</form>';
	if 	($code != false){
		$table_name = $wpdb->prefix . "videoportal_events";
		$myEvent = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $table_name WHERE code = %s",$code ), ARRAY_A);
		if ($myEvent != null) {
		
		$table_name = $wpdb->prefix . "videoportal_videos";
		$example_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE code = %s ORDER BY filename_orig",$code ), ARRAY_A);

		$files=array();
		foreach ($example_data as $object){
			$object['filename_orig'] = str_replace(".mp4", "",$object['filename_orig']);
			$files[] = (array) $object;
		}
		?>

			<script>
			jQuery(document).ready(function(){
			  //$( ".thumbnail" ).bind( "click", function( event ) {});
			  	this.title = <?php echo "'".$myEvent['title']." - FunnyVideoBox '"; ?>;
			  	jQuery('#videocontent').hide();
				jQuery( ".thumbnail" ).on('click', playVideo); 
				setShareLinks(<?php echo '"http://'.get_option('videoportal_event_url').$files[0]['public_key'].'"'; ?> );
			});

			function playVideo() {
				file =  <?php echo '"http://'.get_option('videoportal_video_url').$myEvent['folder'].'/"'; ?> + this.getAttribute("id") +".mp4";
				share =  <?php echo '"http://'.get_option('videoportal_sharing_url').'"'; ?> + this.getAttribute('data-public-key');
				setShareLinks(share);
				//jQuery( '#div-'+this.getAttribute('id') ).removeClass('thumbnaildiv');
				jQuery( '#'+this.getAttribute('data-div-no') ).append(jQuery('#videocontent'));
				//jQuery( '#'+this.getAttribute('id') ).hide();
				videojs('video_player').src(file);
				//alert( "Handler for .click() called." + file);
				videojs('video_player').play();
				
				jQuery('#videocontent').show();
				//jQuery(window).scrollTop(jQuery('#videocontent').position().top);
				jQuery('html, body').animate({ scrollTop: (jQuery('#videocontent').offset().top)}, 'slow');
				//videojs('example_video_1').requestFullScreen()
			}
			function setShareLinks(share) {
				jQuery('#share-facebook').attr('href', 'http://www.facebook.com/sharer.php?u=' + share);
				jQuery('#share-twitter').attr('href', 'http://twitter.com/share?url=' + share + '&text=FunnyVideoBox');
				jQuery('#share-googleplus').attr('href', 'https://plus.google.com/share?url=' + share);
				jQuery('#share-linkedin').attr('href', 'http://www.linkedin.com/shareArticle?mini=true&url=' + share);
				jQuery('#share-mail').attr('href', 'mailto:?Subject=Mein Video aus der FunnyVideoBox&Body=I%20saw%20this%20and%20thought%20of%20you!%20 ' + share);
				jQuery('#share-link').attr('href', share);			
			}
			window.oncontextmenu = function () { return false; }
			</script>
			
			<style type="text/css">
			#share-buttons img {
			width: 35px;
			padding: 5px;
			border: 0;
			box-shadow: 0;
			display: inline;
			}
			#share-buttons {max-width:800px; margin: 0 auto;}
			</style>
			<style type="text/css">
			  .vjs-default-skin { color: #ffffff; }
			  .vjs-default-skin .vjs-play-progress,
			  .vjs-default-skin .vjs-volume-level { background-color: #f5eb00 }
			  .vjs-default-skin .vjs-control-bar { font-size: 100% }
			  .video-js {padding-top: 56.25%}
			  div.titlecontent {width:100%; max-width:800px; margin: 0 auto;}
			  div.videocontent {width:100%; max-width:800px; margin: 0 auto;float:left;height:auto}
			  div.thumbnailcontent {width:100%; max-width:800px; margin: 0 auto;}
			  div.thumbnaildiv {width:33.3%; min-width:200px; float:left;height:auto}
			</style>	
			<div class="content">	
			<div class="titlecontent">	
			<h1><?php echo $myEvent['title']; ?></h1>	
			</div>
			<div class="videocontent" id="videocontent" >	
			<video id="video_player" class="video-js vjs-default-skin vjs-big-play-centered"
			  controls preload="auto" width="auto" height="auto"
			  data-setup='{"example_option":true}'>
			 <source src="<?php echo 'http://'.get_option('videoportal_video_url').$myEvent['folder'].'/'.$files[0]['filename_orig'].'.mp4'; ?>" type='video/mp4' />
			</video>
			
			<!-- I got these buttons from simplesharebuttons.com -->
			<div id="share-buttons">
 			Dieses Video teilen:<br>
			<!-- Facebook -->
			<a id="share-facebook" href="" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/facebook.png" alt="Facebook" /></a>
 
			<!-- Twitter -->
			<a id="share-twitter" href="" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/twitter.png" alt="Twitter" /></a>
 
			<!-- Google+ -->
			<a id="share-googleplus" href="" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/google.png" alt="Google" /></a>
  
			<!-- LinkedIn -->
			<a id="share-linkedin" href="" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/linkedin.png" alt="LinkedIn" /></a>
 
			<!-- Email -->
			<a id="share-mail" href=""><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/email.png" alt="Email" /></a>

			<!-- Link -->
			<a id="share-link" href="" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>img/buffer.png" alt="Link" /></a>
			</div>
			</div>
			<br>
			<div class="thumbnailcontent">
		<?php
		if (count($files) > 0){
		$nof = 1;
		$nod = 1;
		foreach($files as $file){
			echo '<div class="thumbnaildiv"> <img src="http://'.get_option('videoportal_video_url').$myEvent['folder'].'/'.$file['filename_orig'].'.jpg" class="thumbnail" id="'.$file['filename_orig'].'" data-public-key="'.$file['public_key'].'" data-div-no="div-'.$nod.'"></div>';	
			if ($nof%3 == 0){
			echo '<div class="videocontent" id="div-'.$nod.'"></div>';
			$nod = $nod+1;
			} 
			$nof = $nof+1;
			
		}
		} else{
			echo '<p>Leider sind die Videos von diesem Event noch nicht verfügbar. Komm später nochmal vorbei!</p>';
		}
		?>
		</div>
		</div>
		<?php
		} else {
 		 	return "Event Code: ".$code." unbekannt! <br>".$html;
		}

	}else
		return $html;
}
add_shortcode( 'vp_event', 'vp_event_shortcode' );


//[vp_video]
function vp_video_func( $atts ){
	global $wpdb;
	$video_id = get_query_var( 'video_id' );
	if ($video_id != false){
		$table_name = $wpdb->prefix . "videoportal_videos";
		$video = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $table_name WHERE public_key = %s",$video_id ), ARRAY_A);
		if ($video != NULL){
			?>
			</style>
			<style type="text/css">
			  .vjs-default-skin { color: #ffffff; }
			  .vjs-default-skin .vjs-play-progress,
			  .vjs-default-skin .vjs-volume-level { background-color: #f5eb00 }
			  .vjs-default-skin .vjs-control-bar { font-size: 100% }
			  .video-js {padding-top: 56.25%}
			  div.videocontent {width:100%; max-width:800px}
			</style>
			<div class="videocontent">		
			<video id="video_player" class="video-js vjs-default-skin vjs-big-play-centered"
			  controls preload="auto" width="auto" height="auto"
			  data-setup='{"example_option":true}'>
			 <source src="<?php echo 'http://'.get_option('videoportal_video_url').$video['folder'].'/'.$video['filename_orig']; ?>" type='video/mp4' />
			</video>
			</div>	
			<?php	
		}else{
			echo "Das angeforderte Video ist nicht verfügbar";
		}
	}
}
add_shortcode( 'vp_video', 'vp_video_func' );

// Init
function videoportal_init() {
    global $wp;
    
    $wp->add_query_var( 'event_id' );
    $wp->add_query_var( 'video_id' );
    
    // Regel für Name
    add_rewrite_rule( "event/([^/]+)/?$", 'index.php?pagename=event&event_id=$matches[1]', 'top' ); 
    add_rewrite_rule( "video/([^/]+)/?$", 'index.php?pagename=video&video_id=$matches[1]', 'top' ); 
}
add_action( 'init', 'videoportal_init' ); 

// ------------  Database  -----------------------
global $videoportal_db_version;
$videoportal_db_version = "0.1";

function videoportal_install () {
   global $wpdb;
   global $videoportal_db_version;
   $table_name = $wpdb->prefix . "videoportal_events"; 

   $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  code VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  title VARCHAR(100) COLLATE utf8_general_ci,
  folder VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  url VARCHAR(100) COLLATE utf8_general_ci DEFAULT '' NOT NULL,
  password VARCHAR(20) COLLATE utf8_general_ci NOT NULL,
  is_private BOOLEAN DEFAULT true NOT NULL,
  status VARCHAR(20) COLLATE utf8_general_ci NOT NULL,
  order_number VARCHAR(20) COLLATE utf8_general_ci NOT NULL,
  UNIQUE KEY id (id),
  UNIQUE KEY folder (folder),
  UNIQUE KEY url (url),
  UNIQUE KEY code (code),
  KEY order_number (order_number)
    );";
  

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
 
    $table_name = $wpdb->prefix . "videoportal_videos"; 

   $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  code VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  folder VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  public_key VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  filename VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  filename_orig VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  url VARCHAR(100) COLLATE utf8_general_ci DEFAULT '' NOT NULL,
  is_private BOOLEAN DEFAULT false NOT NULL,
  UNIQUE KEY id (id),
  KEY folder (folder),
  KEY code(code),
  UNIQUE KEY filename (filename),
  KEY filename_orig (filename_orig),
  UNIQUE KEY url (url),
  UNIQUE KEY public_key (public_key)
    );";

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
   
   add_option( "videoportal_db_version", $videoportal_db_version );
   add_option( "videoportal_event_url", "www.funnyvideobox.de/event/" );
   add_option( "videoportal_video_folder", "videos" );
   add_option( "videoportal_video_url", "video.funnyvideobox.de/" );
   add_option( "videoportal_sharing_url", "www.funnyvideobox.de/video/" );
}

register_activation_hook( __FILE__, 'videoportal_install' );

// -----------------  Menu -----------------
add_action('admin_menu', 'register_videoportal_menu');

function register_videoportal_menu(){
	add_menu_page( "Video Portal Test", "Video Portal", "manage_options", "videoportal/dashboard.php",'','dashicons-video-alt2' );
	add_submenu_page( "videoportal/dashboard.php", "Events", "Events", "manage_options", "videoportal/event.php", '');
	add_submenu_page( "videoportal/dashboard.php", "Add Event", "Add Event", "manage_options", "videoportal-addevent", 'videoportal_addevent');
	add_submenu_page( "videoportal/dashboard.php", "Settings", "Settings", "manage_options", "videoportal-settings", 'videoportal_settings');
}

function videoportal_settings(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
	<div class="wrap">
	<h2>VideoPortal Einstellungen<h2>
	<?php
	if (isset($_POST['update-option'])){
		update_option("videoportal_event_url",$_POST['url']);
		update_option("videoportal_video_folder",$_POST['video_folder']);
		update_option("videoportal_video_url",$_POST['video_url']);
		update_option("videoportal_sharing_url",$_POST['sharing_url']);
		?>
		<div class="updated"><p><strong><?php _e('Einstellungen gespeichert.', 'videoportal-settings' ); ?></strong></p></div>
		<?php
	}
	?>
	<form action="" method="POST">
	<table class="form-table">
		<tr valign="top">
			<th scope="row">Portal-URL:</th>
			<td><input type="text" name="url" value="<?php echo get_option("videoportal_event_url"); ?>"></td>
		</tr>
		<tr valign="top">
			<th scope="row">Sharing-URL:</th>
			<td><input type="text" name="sharing_url" value="<?php echo get_option("videoportal_sharing_url"); ?>"></td>
		</tr>
		<tr valign="top">
			<th scope="row">Video-Ordner:</th>
			<td><input type="text" name="video_folder" value="<?php echo get_option("videoportal_video_folder"); ?>"></td>
		</tr>
		<tr valign="top">
			<th scope="row">Video-URL:</th>
			<td><input type="text" name="video_url" value="<?php echo get_option("videoportal_video_url"); ?>"></td>
		</tr>
	</table>
	<input type="submit" value="speichern" name="update-option" class="button-primary">
	</form>
	</div>
	<?php	
}

function videoportal_addevent(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
	<div class="wrap">
	<h2>Neues Event anlegen<h2>
	<?php
	global $wpdb;
	if (isset($_POST['create-event'])){
	   $table_name = $wpdb->prefix . "videoportal_events";
	   $result = $wpdb->insert( $table_name, array( 'code' => $_POST['code'], 'folder' => $_POST['folder'], 'password'=> $_POST['password'], 'url' =>  get_option( "videoportal_event_url").$_POST['code'], 'status' => 'erstellt', 'title' => $_POST['title']) );
		if ($result === false){
			?>
			<div class="error"><p><strong><?php _e('Event konnte nicht erstellt werden!', 'videoportal-dashboard' ); ?></strong></p></div>
			<?php
		}else{
			mkdir(get_option('videoportal_video_folder').$_POST['folder']);
			?>
			<div class="updated"><p><strong><?php _e('Event erfolgreich erstellt.', 'videoportal-dashboard' ); ?></strong></p></div>
			<?php
	   }
	}
	?>
	<form action="" method="POST">
	<table class="form-table">
		<tr valign="top">
			<th scope="row">Event-Code</th>
			<td><input type="text" name="code" value="<?php echo wp_generate_password(6,false,false); ?>"></td>
		</tr>
		<tr valign="top">
			<th scope="row">Event-Titel</th>
			<td><input type="text" name="title" value=""></td>
		</tr>
		<tr valign="top">
			<th scope="row">Video-Ordner Name:</th>
			<td><input type="text" name="folder" value="<?php printf('%1$04d', rand(1, 9999)); ?>"></td>
		</tr>
		<tr valign="top">
			<th scope="row">Passwort:</th>
			<td><input type="text" name="password" value="<?php echo wp_generate_password(6,false,false); ?>"></td>
		</tr>
	</table>
	<input type="submit" value="erstellen" name="create-event" class="button-primary">
	</form>
	</div>
	<?php
}

// -----------------------  Video.js ----------------------
		


function videojs_init() {
    if (!is_admin()) {
		wp_register_style( 'videoCSS', plugin_dir_url( __FILE__ ) . 'css/video-js.css' );
		wp_register_script( 'videoJS', plugin_dir_url( __FILE__ ) . 'js/video.js', array( 'jquery' ) );	
		wp_enqueue_style( 'videoCSS' );
		wp_enqueue_script( 'videoJS' );	
    }
}
 
add_action('init', 'videojs_init');	

function html5script() {
	?>
	<script type="text/javascript">
  	videojs.options.flash.swf = "<?php echo plugin_dir_url( __FILE__ ); ?>video-js.swf"
	</script>
	<?php
}

add_action('wp_head', 'html5script');
?>
