<?php
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
?>
<div class="wrap">
<div id="icon-edit" class="icon32"></div>
<h2>Event Informationen</h2>

<style type="text/css">
	.column-id{ width:30px; }
</style>

<?php

if (isset($_GET['action']) && isset($_GET['code'])){
	$code = $_GET['code'];
	$table_name = $wpdb->prefix . "videoportal_events";
	$event = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $table_name WHERE code = %s",$_GET['code'] ), ARRAY_A);
	if ($_GET['action'] == 'edit'){
		if ($event != null) {

		} else {
 		 	echo "Event Code: ".$_GET['code']." unbekannt! <br>".$html;
		}	
	}
	
	if ($_GET['action'] == 'index'){
		$video_dir = get_option('videoportal_video_folder').$event['folder'].'/';
		
		if (file_exists($video_dir)){
			$files = scandir($video_dir);
			$FileList = array();
			foreach ($files as $filename){
				if (substr($filename, -4,4) == ".mp4")
					$FileList[] = $filename;
			}
			sort($FileList);
			if (count($FileList)== 0){
				?>
				<div class="error"><p><strong><?php _e('Keine Videos gefunden!', 'videoportal/event.php' ); ?></strong></p></div>
				<?php		
			}else{
				foreach ($FileList as $filename){
					$data['filename'] = md5($event['folder'].$filename).".mp4";
					$data['is_private'] = false;
					$data['public_key']= md5($filename.uniqid()); 
					$data['folder'] = $event['folder'];
					$data['filename_orig'] = $filename;
					$data['code'] = $code;
					$data['url'] = get_option('videoportal_video_url').$data['filename'];
					$table_name = $wpdb->prefix . "videoportal_videos";
					$wpdb->insert($table_name, $data);
				}
				?>
				<div class="updated"><p><strong><?php _e('Videos erfolgreich indiziert!', 'videoportal/event.php' ); ?></strong></p></div>
				<?php

			}	
		}else{
				?>
				<div class="error"><p><strong><?php _e('Video Verzeichnis existiert nicht!', 'videoportal/event.php' ); ?></strong></p></div>
				<?php	
		}
	}

// ************************************
// *	Video Tabelle				  *
// ************************************

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class My_Example_List_Table extends WP_List_Table {
    function __construct(){
    global $status, $page;

        parent::__construct( array(
            'singular'  => __( 'video', 'mylisttable' ),     //singular name of the listed records
            'plural'    => __( 'videos', 'mylisttable' ),   //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
    ) );
    }

  function column_default( $item, $column_name ) {
    switch( $column_name ) { 
        case 'id':
        case 'filename_orig':
        case 'filename':
        case 'public_key':
    	case 'public':
            return $item[ $column_name ];
        default:
            return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
    }
  }

function get_columns(){
        $columns = array(
            'id' => __( 'ID', 'mylisttable' ),
            'filename_orig'    => __( 'Dateiname original', 'mylisttable' ),
            'filename'      => __( 'Dateiname', 'mylisttable' ),
            'public_key'      => __( 'Sharing Code', 'mylisttable' ),
            'public'      => __( 'Sharing aktiviert', 'mylisttable' )
        );
         return $columns;
    }
function prepare_items() {
  $columns  = $this->get_columns();
  $hidden   = array();
  $sortable = array();
  $this->_column_headers = array( $columns, $hidden, $sortable );
  $this->items = $this->example_data;;
}

} //class

$table_name = $wpdb->prefix . "videoportal_videos";
$example_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE code = %s",$code ), ARRAY_A);

$output=array();
foreach ($example_data as $object){
	$output[] = (array) $object;
}
	
}	
?>

<table class="widefat">
<tbody>
	<tr >
		<td><b>Event Code</b></td>
		<td><?php echo $event['code']; ?></td>   
   </tr>
	<tr class="alternate">
		<td><b>Verzeichnis</b></td>
		<td><?php echo $event['folder']; ?></td>   
   </tr>
	<tr>
		<td><b>Passwort</b></td>
		<td><?php echo $event['password']; ?></td>   
   </tr>
	<tr class="alternate">
		<td><b>URL</b></td>
		<td><a href="http://<?php echo $event['url']; ?>"> <?php echo $event['url']; ?> </a></td>   
   </tr>
	<tr>
		<td><b>Status</b></td>
		<td><?php echo $event['status']; ?></td>   
   </tr>
	<tr class="alternate">
		<td><b>QR-Code</b></td>
		<td><img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=<?php echo "http://".$event['url']; ?>"></td>   
   </tr>
</tbody>
</table>

<table>
<tbody>
	<tr >
		<td>
<form action="" method="GET">
	<input type="hidden" name="page" value="videoportal/event.php">
	<input type="hidden" name="code" value="<?php echo $code ?>">
	<input type="hidden" name="action" value="index">
	<input type="submit" value="Videos indizieren" name="submit" class="button-primary">
</form>		
		</td>
		<td>
<form action="" method="GET">
	<input type="hidden" name="page" value="videoportal/event.php">
	<input type="hidden" name="code" value="<?php echo $code ?>">
	<input type="hidden" name="action" value="edit">
	<input type="submit" value="Bearbeiten" name="submit" class="button-primary">
</form>		
		
		</td>   
   </tr>
</tbody>
</table>

<?php
  $myListTable = new My_Example_List_Table();
  $myListTable->example_data = $output;
  echo '<h2>Videos des Events</h2>'; 
  $myListTable->prepare_items(); 
  $myListTable->display();  
?>

</div>