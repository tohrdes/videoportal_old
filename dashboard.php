<?php
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
?>
<style type="text/css">
	.column-id{ width:30px; }
	.column-folder{ width:80px; }
</style>
<?php

class My_Example_List_Table extends WP_List_Table {


    function __construct(){
    global $status, $page;

        parent::__construct( array(
            'singular'  => __( 'event', 'mylisttable' ),     //singular name of the listed records
            'plural'    => __( 'events', 'mylisttable' ),   //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
    ) );
    }
    
    public $example_data;
    

  function column_default( $item, $column_name ) {
    switch( $column_name ) { 
        case 'id':
        case 'code':
        case 'folder':
        case 'url':
    	case 'status':
            return $item[ $column_name ];
        default:
            return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
    }
  }

function get_columns(){
        $columns = array(
            'id' => __( 'ID', 'mylisttable' ),
            'code'    => __( 'Event Code', 'mylisttable' ),
            'folder'      => __( 'Verzeichnis', 'mylisttable' ),
            'url'      => __( 'Link', 'mylisttable' ),
            'status'      => __( 'Status', 'mylisttable' )
        );
         return $columns;
    }
function prepare_items() {
  $columns  = $this->get_columns();
  $hidden   = array();
  $sortable = array();
  $this->_column_headers = array( $columns, $hidden, $sortable );
  $this->items = $this->example_data;;
}
function column_code($item) {
  $actions = array(
            'edit'      => sprintf('<a href="?page=%s&action=%s&code=%s">Edit</a>','videoportal/event.php','edit',$item['code']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&code=%s">Delete</a>',$_REQUEST['page'],'delete',$item['code']),
        );

  return sprintf('%1$s %2$s', $item['code'], $this->row_actions($actions) );
}
} //class

$table_name = $wpdb->prefix . "videoportal_events";
$example_data = $wpdb->get_results("SELECT * FROM $table_name");

$output=array();
foreach ($example_data as $object){
	$output[] = (array) $object;
}
  $myListTable = new My_Example_List_Table();
  $myListTable->example_data = $output;
  echo '</pre><div class="wrap"><h2>Dashboard</h2>'; 
  $myListTable->prepare_items(); 
  $myListTable->display(); 
  echo '</div>'; 


?>